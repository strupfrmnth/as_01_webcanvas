## Assignment 01 Web Canvas
### How to use
#### Brush
![](https://i.imgur.com/F8QPwPC.png)
1. Click the button to use the brush to draw
![](https://i.imgur.com/qZulV4b.png)

2. When the cursor becomes the icon in the canvas, we can press down the mouse and move to start drawing. If we release the mouse button, then we would stop drawing.
![](https://i.imgur.com/j5o8z9h.png)

#### Eraser
![](https://i.imgur.com/A4L2xA6.png)

1. Click the button to use the brush to draw
![](https://i.imgur.com/jP8m2Fc.png)

2. When the cursor becomes the icon in the canvas, we can press down the mouse and move to start erasing. If we release the mouse button, then we would stop erasing. Besides, we can change the brush size to clear it quickly.
![](https://i.imgur.com/hru4IDp.png)

#### Circle
![](https://i.imgur.com/nIlMYaH.png)
1. Click the button to use the brush to draw![](https://i.imgur.com/rt4q9U2.png)
2. When the cursor becomes the icon in the canvas, we can press down the mouse and move to start drawing. If we release the mouse button, then we would stop drawing. Besides, we can change the color to draw.![](https://i.imgur.com/kIu9gPM.png)

#### Triangle
![](https://i.imgur.com/qlfZ0eP.png)
The drawing method is the same as the Circle.
![](https://i.imgur.com/92sdAhe.png)

#### Rectangle
![](https://i.imgur.com/kyi2kGz.png)
The drawing method is the same as the Circle.
![](https://i.imgur.com/4a9tZVX.png)

#### Line
![](https://i.imgur.com/yoDH1kw.png)
The drawing method is the same as the Circle.
![](https://i.imgur.com/LBr0YHT.png)

#### Text
![](https://i.imgur.com/CB9nVwg.png)
1. Click the button to use the brush to draw
2. When the cursor becomes the "text" in the canvas, we can press down the mouse and will see the input box to input our words.![](https://i.imgur.com/MGmpiEh.png)

3. Then press enter, we can see the words displaying in the canvas.![](https://i.imgur.com/FvuGi6U.png)

#### Reset
![](https://i.imgur.com/zaMGQG7.png)
Press the button, then the canvas will clear.
1. ![](https://i.imgur.com/cFVz9pV.png)
2. ![](https://i.imgur.com/XFtAD8Y.png)


#### Undo
![](https://i.imgur.com/BNARaiJ.png)
Press the button, then the canvas will back to the last step.
1. ![](https://i.imgur.com/EaoleTu.png)

2. ![](https://i.imgur.com/i8x3i8e.png)


#### Redo
![](https://i.imgur.com/g301a3z.png)
Press the button, then the canvas will redo the next step.
1. ![](https://i.imgur.com/V8ude7U.png)
2. ![](https://i.imgur.com/Ngs2NOn.png)

#### Download Image
![](https://i.imgur.com/54jhSrZ.png)
Press the button to download the canvas to image.
1. ![](https://i.imgur.com/iP9EMbL.png)
2. ![](https://i.imgur.com/DiHYglD.png)

#### Upload Image
![](https://i.imgur.com/GnDeQqf.png)
Press the button to upload the image to canvas.
1. ![](https://i.imgur.com/5UFUiHC.png)
2. ![](https://i.imgur.com/MPrGs24.png)

### Function description
1. Listen mouse events and do its corresponding function.
![](https://i.imgur.com/xzG5dDN.png)
2. Each drawXXX() function will create a draw object and using canvas API to draw the shape, brush, eraser, etc. Because the mouse is still moving, we do not finish drawing. Then, we will clear the last drawing. However, it is possible that we will clear the previous drawings; as a result, we use refill() function to re-drawing them.
![](https://i.imgur.com/TVm9l14.png)
3. Using undo() will take away the draw object from the drawobjarray, and throw it to the undoobjarray. Using redo() will take away the undo object from the undoobjarray, and throw it to the drawobjarray. (drawobjarray -> store draw object which will display on the canvas, undoobjarray -> store undo object which will not display on the canvas)
![](https://i.imgur.com/4j8Rrec.png)
4. image
    * download
    Use a tag and get the canvas url to download it.
    * upload 
    Create input element whose type is file, and create file reader to read file by url, then use canvas API to draw the image.
6. bonus: draw line
Use drawline() to draw a line. It records the mousedown and mouseup position and use canvas API like moveTo(), lineTo() to draw a line.
### Gitlab page link
    https://strupfrmnth.gitlab.io/as_01_webcanvas












