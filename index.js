var x = 0;
var y = 0;
var prv_x = -1;
var prv_y = -1;
var lastx = 0;
var lasty = 0;
var draw = false;
var drawobjarray = [], undoobjarray = [];
var drawhandler = [], undohandler = [];
var drawtype = "";
var cursortype = "";
var brushsize = 1;
var brushcolor = "#FF0000";
var text_x, text_y;
function myFunction(e) {
    var currentx = e.clientX;
    var currenty = e.clientY;
    var coor = "Coordinates: (" + currentx + "," + currenty + ")";
    document.getElementById("demo").innerHTML = coor;
    var canvas = document.getElementById("canvas");
    if (canvas.getContext && draw) {
        var ctx = canvas.getContext("2d");
        ctx.fillStyle = brushcolor;
        ctx.strokeStyle = brushcolor;
        ctx.lineWidth = brushsize;
        if(drawtype == "brush") {
            drawbrush(ctx, currentx, currenty);
        }
        else if(drawtype == "eraser") {
            draweraser(ctx, currentx, currenty);
        }
        else if(drawtype == "rect") {
            drawrect(ctx, currentx, currenty);
        }
        else if(drawtype == "tri") {
            drawtri(ctx, currentx, currenty);
        }
        else if(drawtype == "circle") {
            drawcircle(ctx, currentx, currenty);
        }
        else if(drawtype == "line") {
            drawline(ctx, currentx, currenty);
        }
    }
}
function drawbrush(ctx, currentx, currenty) {
    ctx.beginPath();
    ctx.globalCompositeOperation="source-over";
    ctx.moveTo(lastx, lasty);
    ctx.lineTo(currentx, currenty);
    ctx.stroke();
    var drawobj = {};
    drawobj["type"] = "brush";
    drawobj["brushsize"] = brushsize;
    drawobj["brushcolor"] = brushcolor;
    drawobj["moveTo_x"] = lastx;
    drawobj["moveTo_y"] = lasty;
    drawobj["lineTo_x"] = currentx;
    drawobj["lineTo_y"] = currenty;
    drawobjarray.push(drawobj);
    lastx = currentx;
    lasty = currenty;
}
function draweraser(ctx, currentx, currenty) {
    ctx.beginPath();
    ctx.globalCompositeOperation="destination-out";
    ctx.arc(lastx, lasty, ctx.lineWidth, 0, Math.PI*2, false);
    ctx.fill();
    var drawobj = {};
    drawobj["type"] = "eraser";
    drawobj["brushsize"] = brushsize;
    drawobj["brushcolor"] = brushcolor;
    drawobj["moveTo_x"] = lastx;
    drawobj["moveTo_y"] = lasty;
    drawobj["lineTo_x"] = currentx;
    drawobj["lineTo_y"] = currenty;
    drawobjarray.push(drawobj);
    lastx = currentx;
    lasty = currenty;
}
function drawrect(ctx, currentx, currenty) {
    var rectobj = CreateDrawobj(x, y, currentx, currenty);
    var width = rectobj["width"];
    var height = rectobj["height"];
    var startx = rectobj["startx"];
    var starty = rectobj["starty"];
    if(prv_x == -1 && prv_y == -1) {
        ctx.fillRect(startx, starty, width, height);
    }
    else {
        var clearrectobj = CreateDrawobj(x, y, prv_x, prv_y);
        var clearwidth = clearrectobj["width"];
        var clearheight = clearrectobj["height"];
        var clearx = clearrectobj["startx"];
        var cleary = clearrectobj["starty"];
        ctx.clearRect(clearx, cleary-1, clearwidth+1, clearheight+1);
        refill();
        ctx.globalCompositeOperation="source-over";
        ctx.fillRect(startx, starty, width, height);
    }
    prv_x = currentx;
    prv_y = currenty;
}
function drawtri(ctx, currentx, currenty) {
    var triobj = CreateDrawobj(x, y, currentx, currenty);
    var moveTo_x = triobj["startx"] + triobj["width"]/2;
    var moveTo_y = triobj["starty"];
    var lineTo_y = triobj["starty"] + triobj["height"];
    var lineTo_x1 = triobj["startx"];
    var lineTo_x2 = triobj["startx"] + triobj["width"];
    if(prv_x == -1 && prv_y == -1) {
        ctx.beginPath();
        ctx.moveTo(moveTo_x, moveTo_y);
        ctx.lineTo(lineTo_x1,lineTo_y);
        ctx.lineTo(lineTo_x2,lineTo_y);
        ctx.fill();
    }
    else {
        var cleartriobj = CreateDrawobj(x, y, prv_x, prv_y);
        var clearwidth = cleartriobj["width"];
        var clearheight = cleartriobj["height"];
        var clearx = cleartriobj["startx"];
        var cleary = cleartriobj["starty"];
        ctx.beginPath();
        ctx.globalCompositeOperation="destination-out";
        ctx.clearRect(clearx, cleary-1, clearwidth+1, clearheight+1);
        refill()
        ctx.beginPath();
        ctx.globalCompositeOperation="source-over";
        ctx.moveTo(moveTo_x, moveTo_y);
        ctx.lineTo(lineTo_x1,lineTo_y);
        ctx.lineTo(lineTo_x2,lineTo_y);
        ctx.fill();
    }
    prv_x = currentx;
    prv_y = currenty;
}
function drawcircle(ctx, currentx, currenty) {
    var circleobj = CreateDrawobj(x, y, currentx, currenty);
    var radius = (circleobj["width"]>circleobj["height"]) ? circleobj["width"]/2 : circleobj["height"]/2;
    var arc_x = circleobj["startx"] + radius;
    var arc_y = circleobj["starty"] + radius;
    if(prv_x == -1 && prv_y == -1) {
        ctx.beginPath();
        ctx.globalCompositeOperation="source-over";
        ctx.arc(arc_x, arc_y, radius, 0, Math.PI*2, false);
        ctx.fill();
    }
    else {
        var clearcircleobj = CreateDrawobj(x, y, prv_x, prv_y);
        var clearwidth = clearcircleobj["width"];
        var clearheight = clearcircleobj["height"];
        var clearlength = (clearwidth>clearheight) ? clearwidth : clearheight;
        var clearx = clearcircleobj["startx"];
        var cleary = clearcircleobj["starty"];
        ctx.beginPath();
        ctx.globalCompositeOperation="destination-out";
        ctx.clearRect(clearx, cleary, clearlength+1, clearlength+1);
        refill();
        ctx.beginPath();
        ctx.globalCompositeOperation="source-over";
        ctx.arc(arc_x, arc_y, radius, 0, Math.PI*2, false);
        ctx.fill();
    }
    prv_x = currentx;
    prv_y = currenty;
}
function drawline(ctx, currentx, currenty) {
    ctx.beginPath();
    ctx.globalCompositeOperation="source-over";
    if(prv_x == -1 && prv_y == -1) {
        ctx.moveTo(x, y);
        ctx.lineTo(currentx, currenty);
        ctx.stroke();
    }
    else {
        var clearrectobj = CreateDrawobj(x, y, prv_x, prv_y);
        var clearwidth = clearrectobj["width"];
        var clearheight = clearrectobj["height"];
        var clearx = clearrectobj["startx"];
        var cleary = clearrectobj["starty"];
        clearall();
        // ctx.globalCompositeOperation="destination-out";
        // ctx.clearRect(clearx-1, cleary-1, clearwidth+2, clearheight+2);
        refill();
        ctx.globalCompositeOperation="source-over";
        ctx.moveTo(x, y);
        ctx.lineTo(currentx, currenty);
        ctx.stroke();
    }
    prv_x = currentx;
    prv_y = currenty;
}
function startdraw(e) {
    x = e.clientX;
    y = e.clientY;
    lastx = x;
    lasty = y;
    draw = true;
    if(drawtype == "text") {
        var textobj = document.getElementById("draw-text");
        textobj.style.top = y.toString() + "px";
        textobj.style.left = x.toString() + "px";
        textobj.style.visibility = "visible";
        text_x = x;
        text_y = y;
    }
    var handler = {};
    handler["start_index"] = drawobjarray.length;
    handler["end_index"] = 0;
    drawhandler.push(handler);
}
function stopdraw(e) {
    draw = false;
    if(drawtype == "rect" || drawtype == "tri" || drawtype == "circle") {
        var drawobj = CreateDrawobj(x, y, prv_x, prv_y, drawtype);
        drawobj["brushcolor"] = brushcolor;
        drawobjarray.push(drawobj);
        prv_x = -1;
        prv_y = -1;
    }
    else if(drawtype == "line") {
        var drawobj = {};
        drawobj["type"] = "line";
        drawobj["brushsize"] = brushsize;
        drawobj["brushcolor"] = brushcolor;
        drawobj["moveTo_x"] = x;
        drawobj["moveTo_y"] = y;
        drawobj["lineTo_x"] = prv_x;
        drawobj["lineTo_y"] = prv_y;
        drawobjarray.push(drawobj);
        prv_x = -1;
        prv_y = -1;
    }
    if(drawtype != "text") {
        drawhandler[drawhandler.length-1]["end_index"] = drawobjarray.length-1;
    }
}
function CreateDrawobj(x, y, currentx, currenty, type="rect") {
    var drawobj = {};
    var width, height, startx, starty;
    if(currentx >= x && currenty >= y) {
        width = currentx - x;
        height = currenty - y;
        startx = x;
        starty = y;
    }
    else if(currentx >= x && currenty < y) {
        width = currentx - x;
        height = y - currenty;
        startx = x;
        starty = y - height;
    }
    else if(currentx < x && currenty >= y) {
        width = x - currentx;
        height = currenty - y;
        startx = x - width;
        starty = y;
    }
    else {
        width = x - currentx;
        height = y - currenty;
        startx = x - width;
        starty = y - height;
    }
    drawobj["type"] = type;
    drawobj["brushsize"] = brushsize;
    drawobj["brushcolor"] = brushcolor;
    drawobj["width"] = width;
    drawobj["height"] = height;
    drawobj["startx"] = startx;
    drawobj["starty"] = starty;
    return drawobj;
}
function refill() {
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    var origin_color = ctx.fillStyle;
    var origin_line_width = ctx.lineWidth;
    var origin_font = ctx.font;
    for(var i = 0; i < drawobjarray.length; i++) {// refill other drawobj
        if(drawobjarray[i]["type"] == "rect") {
            ctx.globalCompositeOperation="source-over";
            ctx.fillStyle = drawobjarray[i]["brushcolor"];
            var start_y = drawobjarray[i]["starty"];
            var start_x = drawobjarray[i]["startx"];
            ctx.fillRect(start_x, start_y, drawobjarray[i]["width"], drawobjarray[i]["height"]);
        }
        else if(drawobjarray[i]["type"] == "tri") {
            var moveTo_x = drawobjarray[i]["startx"] + drawobjarray[i]["width"]/2;
            var moveTo_y = drawobjarray[i]["starty"];
            var lineTo_y = drawobjarray[i]["starty"] + drawobjarray[i]["height"];
            var lineTo_x1 = drawobjarray[i]["startx"];
            var lineTo_x2 = drawobjarray[i]["startx"] + drawobjarray[i]["width"];
            ctx.beginPath();
            ctx.globalCompositeOperation="source-over";
            ctx.fillStyle = drawobjarray[i]["brushcolor"];
            ctx.moveTo(moveTo_x, moveTo_y);
            ctx.lineTo(lineTo_x1,lineTo_y);
            ctx.lineTo(lineTo_x2,lineTo_y);
            ctx.fill();
        }
        else if(drawobjarray[i]["type"] == "circle") {
            var radius = (drawobjarray[i]["width"]>drawobjarray[i]["height"]) ? drawobjarray[i]["width"]/2 : drawobjarray[i]["height"]/2;
            var arc_x = drawobjarray[i]["startx"] + radius;
            var arc_y = drawobjarray[i]["starty"] + radius;
            ctx.beginPath();
            ctx.globalCompositeOperation="source-over";
            ctx.fillStyle = drawobjarray[i]["brushcolor"];
            ctx.arc(arc_x, arc_y, radius, 0, Math.PI*2, false);
            ctx.fill();
        }
        else if(drawobjarray[i]["type"] == "brush") {
            ctx.beginPath();
            ctx.globalCompositeOperation="source-over";
            ctx.strokeStyle = drawobjarray[i]["brushcolor"];
            ctx.fillStyle = drawobjarray[i]["brushcolor"];
            ctx.lineWidth = drawobjarray[i]["brushsize"];
            ctx.moveTo(drawobjarray[i]["moveTo_x"], drawobjarray[i]["moveTo_y"]);
            ctx.lineTo(drawobjarray[i]["lineTo_x"], drawobjarray[i]["lineTo_y"]);
            ctx.stroke();
        }
        else if(drawobjarray[i]["type"] == "eraser") {
            ctx.beginPath();
            ctx.globalCompositeOperation="destination-out";
            ctx.strokeStyle = drawobjarray[i]["brushcolor"];
            ctx.fillStyle = drawobjarray[i]["brushcolor"];
            ctx.lineWidth = drawobjarray[i]["brushsize"];
            ctx.arc(drawobjarray[i]["moveTo_x"], drawobjarray[i]["moveTo_y"], ctx.lineWidth, 0, Math.PI*2, false);
            ctx.fill();
        }
        else if(drawobjarray[i]["type"] == "text") {
            ctx.beginPath();
            ctx.globalCompositeOperation="source-over";
            var refill_fontsize = drawobjarray[i]["font-size"];
            var refill_fontfamily = drawobjarray[i]["font-family"];
            var refill_brushcolor = drawobjarray[i]["brushcolor"];
            var refill_textvalue = drawobjarray[i]["text-value"];
            var refill_text_x = drawobjarray[i]["text_x"];
            var refill_text_y = drawobjarray[i]["text_y"];
            ctx.font = refill_fontsize + "px " + refill_fontfamily;
            ctx.fillStyle = refill_brushcolor;
            ctx.fillText(refill_textvalue, refill_text_x, refill_text_y);
        }
        else if(drawobjarray[i]["type"] == "img") {
            clearall();
            var img = drawobjarray[i]["image"];
            img.src = drawobjarray[i]["image_src"];
            img.onload = function(){
                ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            }
        }
        else if(drawobjarray[i]["type"] == "clear") {
            clearall();
        }
        else if(drawobjarray[i]["type"] == "line") {
            ctx.fillStyle = drawobjarray[i]["brushcolor"];
            ctx.strokeStyle = drawobjarray[i]["brushcolor"];
            ctx.lineWidth = drawobjarray[i]["brushsize"];
            ctx.beginPath();
            ctx.globalCompositeOperation="source-over";
            ctx.moveTo(drawobjarray[i]["moveTo_x"], drawobjarray[i]["moveTo_y"]);
            ctx.lineTo(drawobjarray[i]["lineTo_x"], drawobjarray[i]["lineTo_y"]);
            ctx.stroke();
        }
    }
    ctx.strokeStyle = origin_color;
    ctx.fillStyle = origin_color;
    ctx.lineWidth = origin_line_width;
    ctx.font = origin_font;
}
function choosedrawtype(type) {
    drawtype = type;
    var canvas = document.getElementById("canvas");
    if(drawtype == "text") {
        canvas.style.cursor = "text";
    }
    else {
        canvas.style.cursor = "url(./img/" + type + ".png), auto";
    }
}
function changebrushsize() {
    brushsize = document.getElementById("brush-size").value;
    document.getElementById("brush-size-label").innerHTML = "brush size: " + brushsize.toString();
}
function changebrushcolor() {
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    brushcolor = document.getElementById("brush-color").value;
    ctx.fillStyle = brushcolor;
    ctx.strokeStyle = brushcolor;
}
function clear_allcanvas() {
    var handler = {};
    handler["start_index"] = drawobjarray.length;
    handler["end_index"] = 0;
    drawhandler.push(handler);
    var drawobj = {};
    drawobj["type"] = "clear";
    drawobjarray.push(drawobj);
    drawhandler[drawhandler.length-1]["end_index"] = drawobjarray.length-1;
    clearall();
}
function clearall() {
    var canvas = document.getElementById("canvas");
    if(canvas.getContext) {
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
}
function drawtext() {
    if(event.keyCode == "13") {
        var textobj = document.getElementById("draw-text");
        var ctx = document.getElementById("canvas").getContext("2d");
        var fontsize = document.getElementById("font-size").value;
        var fontfamily = document.getElementById("font-family").value;
        var textvalue = textobj.value
        ctx.font = fontsize + "px " + fontfamily;
        ctx.fillStyle = brushcolor;
        ctx.fillText(textvalue, text_x, text_y);
        textobj.style.visibility = "hidden";
        textobj.value = "";
        var drawobj = {};
        drawobj["type"] = "text";
        drawobj["font-size"] = fontsize;
        drawobj["font-family"] = fontfamily;
        drawobj["brushcolor"] = brushcolor;
        drawobj["text-value"] = textvalue;
        drawobj["text_x"] = text_x;
        drawobj["text_y"] = text_y;
        drawobjarray.push(drawobj);
        draw = false;
        drawhandler[drawhandler.length-1]["end_index"] = drawobjarray.length-1;
    }
}
function undo() {
    if(drawhandler.length!=0 && drawobjarray.length!=0) {
        var start_index = drawhandler[drawhandler.length-1]["start_index"];
        var end_index = drawhandler[drawhandler.length-1]["end_index"];
        var pop_times = end_index - start_index + 1;
        var undo_start_index = undoobjarray.length;
        for(var i = start_index; i <= end_index; i++) {
            undoobjarray.push(drawobjarray[i]);
        }
        for(var i = 0; i < pop_times; i++) {
            drawobjarray.pop();
        }
        var handler = {};
        handler["start_index"] = undo_start_index;
        handler["end_index"] = undo_start_index + pop_times - 1;
        undohandler.push(handler);
        drawhandler.pop();
        clearall();
        refill();
    }
}
function redo() {
    if(undohandler.length!=0 && undoobjarray.length!=0) {
        var start_index = undohandler[undohandler.length-1]["start_index"];
        var end_index = undohandler[undohandler.length-1]["end_index"];
        var pop_times = end_index - start_index + 1;
        var redo_start_index = drawobjarray.length;
        for(var i = start_index; i <= end_index; i++) {
            drawobjarray.push(undoobjarray[i]);
        }
        for(var i = 0; i < pop_times; i++) {
            undoobjarray.pop();
        }
        var handler = {};
        handler["start_index"] = redo_start_index;
        handler["end_index"] = redo_start_index + pop_times - 1;
        drawhandler.push(handler);
        undohandler.pop();
        clearall();
        refill();    
    }
}
function uploadimg() {
    var input = document.createElement("input");
    input.addEventListener("change", readimg);
    input.type = "file";
    input.accept = "image/*";
    input.click();
}
function readimg() {
    var file = this.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function(e){
        ImgToCanvas(this.result);
    }
}
function ImgToCanvas(imgdata) {
    clearall();
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext('2d');
    var img = new Image;
    img.src = imgdata;
    img.onload = function(){
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    }
    drawtype = "img";
    var handler = {};
    handler["start_index"] = drawobjarray.length;
    handler["end_index"] = 0;
    drawhandler.push(handler);
    var drawobj = {};
    drawobj["type"] = drawtype;
    drawobj["image"] = img;
    drawobj["image_src"] = imgdata;
    drawobjarray.push(drawobj);
    stopdraw();
}